import React from "react";
import "./Item.scss";
import {ReactComponent as Arrow} from "./img/arrow.svg";

const Item = ({title, price, image_url}) => {
    return (
        <div className="Item">
            <div className="image-wrapper">
                <img src={`${process.env.PUBLIC_URL}/${image_url}`} alt=""/>
                <div className="details">
                    <div className="text">
                        <div>from</div>
                        <div className="price" data-currency="€">{price}</div>
                    </div>
                    <a href="/">View details  <Arrow /></a>
                </div>
            </div>
            <h4 className="text-center">{title}</h4>
        </div>
    )
}

export default Item;