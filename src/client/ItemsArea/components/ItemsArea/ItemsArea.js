import React from "react";
import Item from "../Item";
import "./ItemsArea.scss";

const ItemsArea = ({items}) => {
    const itemsList = items.map( item => {
        return (
            <div className="col-md-4 mb-4">
                <Item {...item} />
            </div>
        )
    })

    return (
        <div className="container">
            <div className="row">
                { itemsList }
            </div>
        </div>
    )
}

export default ItemsArea;