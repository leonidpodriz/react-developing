import React from "react";
import "./Footer.scss";

const Footer = ({year, company}) => {
    return (
        <footer className="Footer">
            &copy; {year} {company}. All rights reserved.
        </footer>
    )
}

export default Footer;