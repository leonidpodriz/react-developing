import React from "react";
import Logo from "../../../../shared/Logo";
import Contacts from "../../../../shared/Contacts";

const Header = (props) => {
    const {holiday_place, phone, email} = props;

    return (
        <div className="container py-4 mb-4">
            <div className="row justify-content-between">
                <div className="col">
                    <Logo holiday_place={holiday_place} />
                </div>
                <div className="col">
                    <Contacts phone={phone} email={email} />
                </div>
            </div>
        </div>
    )
}

export default Header;