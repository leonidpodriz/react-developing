import React from "react";
import Header from "../../../client/Header/components/Header";
import ItemsArea from "../../../client/ItemsArea/components/ItemsArea";
import Footer from "../../../client/Footer/components/Footer";
import "./App.scss";

const globalState = {
    company: "Copyright Milan Holidays, Inc",
    current_holiday_place: "Milan",
    contacts: {
        phone: "+39 338 709 0880",
        email: "tours@milanoholidays.com",
    },
    items: [
        {
            title: "Walking Tours",
            price: 50,
            image_url: "/img/image.png"
        },
        {
            title: "Art & Architecture",
            price: 50,
            image_url: "/img/image-1.png"
        },
        {
            title: "Bars & Nightlife",
            price: 50,
            image_url: "/img/image-2.png"
        },
        {
            title: "Food Tours",
            price: 50,
            image_url: "/img/image-3.png"
        },
        {
            title: "Sightseeing Tours",
            price: 50,
            image_url: "/img/image-4.png"
        },
        {
            title: "Shopping Tours",
            price: 50,
            image_url: "/img/image-5.png"
        },
    ],
}

const App = () => {
    return (
        <div className="App">
            <Header holiday_place={globalState.current_holiday_place} {...globalState.contacts}/>
            <ItemsArea items={globalState.items}/>
            <Footer year={new Date().getFullYear()} company={globalState.company}/>
        </div>
    )
}

export default App;